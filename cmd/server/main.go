package main

import (
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func main() {
	host := os.Getenv("API_HOST")
	port := os.Getenv("API_PORT")
	config := fmt.Sprintf("%s:%s", host, port)

	router := gin.Default()
	router.POST("/", createContentHandler)
	router.GET("/", getContentHandler)
	router.PUT("/:id", updateContentHandler)
	router.DELETE("/:id", deleteContentHandler)

	log.Printf("Running at %s\n", config)
	router.Run(config)
}
