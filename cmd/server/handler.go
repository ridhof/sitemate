package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Content struct {
	Id			int		`json:"id"`
	Title		string	`json:"title"`
	Description	string	`json:"description"`
}

func pingHandler(c *gin.Context) {
	content := Content{}
	c.IndentedJSON(
		http.StatusOK,
		gin.H{"message": "Hello World!", "data": content},
	)
}

func createContentHandler(c *gin.Context) {
	content := Content{}
	if err := c.BindJSON(&content); err != nil {
		c.IndentedJSON(http.StatusBadRequest, nil)
		return
	}

	log.Println(content)
	c.IndentedJSON(
		http.StatusOK,
		gin.H{"data": content},
	)
}

func updateContentHandler(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		c.IndentedJSON(http.StatusBadRequest, nil)
		return
	}

	content := Content{}
	if err := c.BindJSON(&content); err != nil {
		c.IndentedJSON(http.StatusBadRequest, nil)
		return
	}
	content.Id = id

	log.Println(id, content)
	c.IndentedJSON(
		http.StatusOK,
		gin.H{"data": content},
	)
}

func deleteContentHandler(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Fatal(err)
		c.IndentedJSON(http.StatusBadRequest, nil)
		return
	}
	content := Content{Id: id}
	log.Println(content)
	
	c.IndentedJSON(
		http.StatusOK,
		gin.H{"data": content},
	)
}

func getContentHandler(c *gin.Context) {
	content := []Content{Content{}, Content{}}
	
	c.IndentedJSON(
		http.StatusOK,
		gin.H{"data": content},
	)
}
