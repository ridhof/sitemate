package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
)

func main() {
	fetch("GET", 0, nil)
	fetch("POST", 0, []byte(`{"title":"My Title", "description": "my description"}`))
	fetch("PUT", 1, []byte(`{"title":"My Title", "description": "my description"}`))
	fetch("DELETE", 1, nil)
}

func fetch(method string, id int, data []byte) {
	host := os.Getenv("API_HOST")
	port := os.Getenv("API_PORT")
	config := fmt.Sprintf("http://%s:%s", host, port)
	if !(method == "GET" || method == "POST") {
		config = fmt.Sprintf("%s/%v", config, id)
	}

	c := http.Client{}
	// jsonBody := []byte(`{"title": "my title!", "description": "my desc"}`)
 	bodyReader := bytes.NewReader(data)
	req, err := http.NewRequest(method, config, bodyReader)
	if err != nil {
		panic(err)
		os.Exit(1)
	}

	req.Header.Add("Accept", `application/json`)
	resp, err := c.Do(req)
	if err != nil {
		panic(err)
		os.Exit(1)
	}

	fmt.Println(resp.StatusCode)
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
		os.Exit(1)
	}

	fmt.Printf("%s\n", body)
}
